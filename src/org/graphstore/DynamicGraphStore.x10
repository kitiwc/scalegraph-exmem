/*
* This is part of ScaleGraph-exmem.
*
* Copyright (C) 2015-2016 The GraphCREST Project, Tokyo Institute of Technology.
*
* This file is licensed to You under the Eclipse Public License (EPL);
* You may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* http://www.opensource.org/licenses/eclipse-1.0.php
*/

package graphstore;

import x10.util.Team;

// import org.scalegraph.Config;
// import org.scalegraph.api.DegreeDistribution;
// import org.scalegraph.graph.Graph;
// import org.scalegraph.graph.GraphGenerator;
// import org.scalegraph.io.CSV;
// import org.scalegraph.io.NamedDistData;
// import org.scalegraph.util.random.Random;
// import org.scalegraph.util.tuple.*;

// import org.scalegraph.util.DistMemoryChunk;
// import org.scalegraph.util.Parallel;

// import x10.compiler.Inline;
// import x10.compiler.Ifdef;
// import x10.compiler.Ifndef;
import x10.compiler.Native;
import x10.compiler.NativeRep;
import x10.compiler.NativeCPPInclude;
import x10.compiler.NativeCPPCompilationUnit;

import x10.compiler.NativeCPPOutputFile;

@NativeCPPInclude("DynamicGraphStoreNative.h")
@NativeRep("c++", "DynamicGraphStoreNative<#E>*", "DynamicGraphStoreNative<#E>*", null)
struct DynamicGraphStoreNativeDummy[E] {

}

//@NativeRep("c++", "DynamicGraphStoreNative<#E>*", "DynamicGraphStoreNative<#E>*", null)
@NativeCPPInclude("DynamicGraphStoreNativeDummy.h")
final class DynamicGraphStore [E] {
  transient var dynamicGraphStoreNativeDummy :DynamicGraphStoreNativeDummy[E];

 // @Native("c++", "(#this)->insert(#src, #dst, #wg)")
 // public native def insert(src :Long, dst :Long, wg :E) :Int;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->insert(#src, #dst, #wg)")
 public native def insert(src :Long, dst :Long, wg :E) :Boolean;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->insert(#src, #dst)")
 public native def insert(src :Long, dst :Long) :Boolean;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->degree(#src)")
 public native def degree(src :Long) :Long;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->vertices()")
 public native def vertices() :void;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->get_next_vertex()")
 public native def get_next_vertex() :Long;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->edges(#src)")
 public native def edges(src :Long) :void;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->get_next_edge()")
 public native def get_next_edge() :Long;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->set_vertex_property(#prop)")
 public native def set_vertex_property(prop :E) :void;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->get_vertex_property()")
 public native def get_vertex_property() :E;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->set_edge_property(#prop)")
 public native def set_edge_property(prop :E) :void;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->get_edge_property()")
 public native def get_edge_property() :E;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->num_vertices()")
 public native def num_vertices() :Long;

 @Native("c++", "(#this)->FMGL(dynamicGraphStoreNativeDummy)->num_edges()")
 public native def num_edges() :Long;

 @Native("c++", "new DynamicGraphStoreNative<#E>(#store_id)")
 static native def make[E](store_id :Long) :DynamicGraphStoreNativeDummy[E];

 public def this(store_id :Long) {
  dynamicGraphStoreNativeDummy = make[E](store_id);
 }

}
