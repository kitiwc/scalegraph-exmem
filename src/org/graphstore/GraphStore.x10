/*
* This is part of ScaleGraph-exmem.
*
* Copyright (C) 2015-2016 The GraphCREST Project, Tokyo Institute of Technology.
*
* This file is licensed to You under the Eclipse Public License (EPL);
* You may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* http://www.opensource.org/licenses/eclipse-1.0.php
*/

package graphstore;

import x10.io.Console;
import org.scalegraph.Config;

import org.scalegraph.util.MemoryChunk;
import org.scalegraph.util.tuple.Tuple2;
import org.scalegraph.util.tuple.Tuple3;

import graphstore.DynamicGraphStore;

public class GraphStore[E] {

  var mDynamicGraphStore :DynamicGraphStore[E];

  public def set(off :MemoryChunk[Long], ver :MemoryChunk[Long], value :MemoryChunk[E]) {
    val numLocalVertices = off.size() - 1;
    for(vid in 0..(numLocalVertices-1)) {
      for(i in off(vid)..(off(vid + 1) - 1)) {
        mDynamicGraphStore.insert(vid, ver(i), value(i));
      }
    }
  }

  public def set(off :MemoryChunk[Long], ver :MemoryChunk[Long]) {
    val numLocalVertices = off.size() - 1;
    for(vid in 0..(numLocalVertices-1)) {
      for(i in off(vid)..(off(vid + 1) - 1)) {
        mDynamicGraphStore.insert(vid, ver(i));
      }
    }
  }

  public def insert(src :Long, dst :Long, wg :E) :Boolean {
    return mDynamicGraphStore.insert(src, dst, wg);
  }


  public def degree(srcid :Long) :Long {
    return mDynamicGraphStore.degree(srcid);
  }

  public def vertices() {
    mDynamicGraphStore.vertices();
  }

  public def get_next_vertex() :Long {
    return mDynamicGraphStore.get_next_vertex();
  }

  public def edges(srcid :Long) {
    mDynamicGraphStore.edges(srcid);
  }

  public def get_next_edge() :Long {
    return mDynamicGraphStore.get_next_edge();
  }

  public def get_vertex_property() :E {
   return mDynamicGraphStore.get_vertex_property();
  }

  public def set_vertex_property(prop :E) {
    mDynamicGraphStore.set_vertex_property(prop);
  }

  public def get_edge_property() :E {
    return mDynamicGraphStore.get_edge_property();
  }

  public def set_edge_property(prop :E) {
    mDynamicGraphStore.set_edge_property(prop);
  }

  public def get_adjedges(srcid :Long) :MemoryChunk[Long] {
    val deg = degree(srcid);
    val outedges = MemoryChunk.make[Long](deg);
    edges(srcid);
    for(i in 0..(deg-1)) {
      outedges(i) = get_next_edge();
    }

    return outedges;
  }

  public def get_adjedges_with_property(srcid :Long) :Tuple2[MemoryChunk[Long], MemoryChunk[E]] {
    val deg = degree(srcid);
    // Console.OUT.println("get_adjedges_with_property " + srcid + " deg " + deg);

    val outedges = MemoryChunk.make[Long](deg);
    val properties = MemoryChunk.make[E](deg);
    if (deg > 0) {
      edges(srcid);
      for (i in 0..(deg-1)) {
        val p = get_edge_property();
        properties(i) = p;
        val e = get_next_edge();
        outedges(i) = e;
      }
    }

    return new Tuple2[MemoryChunk[Long],MemoryChunk[E]](outedges, properties);
  }

  public def num_vertices() :Long {
    return mDynamicGraphStore.num_vertices();
  }

  public def num_edges() :Long {
    return mDynamicGraphStore.num_edges();
  }

  public def this() {
    val sw = Config.get().stopWatch();
    sw.lap("GraphStore this()");
  }

  public def this(store_id :Long) {
    val sw = Config.get().stopWatch();
    sw.lap("GraphStore this(store_id) " + store_id);
    mDynamicGraphStore = new DynamicGraphStore[E](store_id);
  }

  public static def make[E](store_id :Long) : GraphStore[E] {
    // val sw = Config.get().stopWatch();
    // sw.lap("GraphStore make(store_id) " + store_id);
    return new GraphStore[E](store_id);
  }

}
