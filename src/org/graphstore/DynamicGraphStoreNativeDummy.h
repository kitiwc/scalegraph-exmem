/*
* This is part of ScaleGraph-exmem.
*
* Copyright (C) 2015-2016 The GraphCREST Project, Tokyo Institute of Technology.
*
* This file is licensed to You under the Eclipse Public License (EPL);
* You may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* http://www.opensource.org/licenses/eclipse-1.0.php
*/

#include<graphstore/DynamicGraphStoreNative.h>