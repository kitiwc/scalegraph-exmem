/*
* This is part of ScaleGraph-exmem.
*
* Copyright (C) 2015-2016 The GraphCREST Project, Tokyo Institute of Technology.
*
* This file is licensed to You under the Eclipse Public License (EPL);
* You may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* http://www.opensource.org/licenses/eclipse-1.0.php
*/

#ifndef DYNAMICGRAPHSTORENATIVE_HPP_INCLUDED
#define DYNAMICGRAPHSTORENATIVE_HPP_INCLUDED


#include <iostream>
#include <fstream>

#include <x10rt.h>

#include <boost/interprocess/managed_mapped_file.hpp>

#include <havoqgt/graphstore/graphstore_utilities.hpp>
#include <havoqgt/graphstore/baseline/baseline.hpp>
#include <havoqgt/graphstore/degawarerhh/degawarerhh.hpp>

using mapped_file_type     = boost::interprocess::managed_mapped_file;
using segment_manager_type = boost::interprocess::managed_mapped_file::segment_manager;

inline void* operator new(std::size_t sz)
{
  // std::cout << "global op new called, size = " << sz << std::endl;
  // void* mem = x10aux::alloc<void*>(sz, true);
  // return mem;
  return std::malloc(sz);
}

inline void operator delete(void *p)
{
  // std::cout << "delete called but do nothing" << std::endl;
}

template <typename E>
class DynamicGraphStoreNative
{

 public:
  // RTT_H_DECLS_CLASS
  // static const ::x10aux::RuntimeType* getRTT() {};

  using vertex_id_type        = uint64_t;
  using edge_property_type    = E;
  using vertex_property_type  = E;
#if 1
  using graphstore_type       = graphstore::degawarerhh<vertex_id_type,
                                                              vertex_property_type,
                                                              edge_property_type,
                                                              segment_manager_type>;
#else
  using graphstore_type       = graphstore::graphstore_baseline<vertex_id_type,
                                                             vertex_property_type,
                                                             edge_property_type,
                                                             segment_manager_type>;
#endif

  DynamicGraphStoreNative() :
  m_mmap_manager(nullptr),
  m_graphstore(nullptr),
  m_vertex_iterator(graphstore_type::vertices_end()),
  m_adjacent_edge_iterator(graphstore_type::adjacent_edge_end(vertex_id_type()))
  { };

  explicit DynamicGraphStoreNative(int64_t store_id) :
  m_mmap_manager(nullptr),
  m_graphstore(nullptr),
  m_vertex_iterator(graphstore_type::vertices_end()),
  m_adjacent_edge_iterator(graphstore_type::adjacent_edge_end(vertex_id_type()))
  {

    if (store_id  >= 0) {
      size_t graph_capacity = std::pow(2, 30);
      std::stringstream fname_local_segmentfile;
      fname_local_segmentfile << "/dev/shm/graph_havoqgt_" << store_id;
      graphstore::utility::interprocess_mmap_manager::delete_file(fname_local_segmentfile.str());
      std::cout << "new interprocess_mmap_manager" << std::endl;
      m_mmap_manager = new graphstore::utility::interprocess_mmap_manager(fname_local_segmentfile.str(), graph_capacity);

      /// --- get a segment_manager --- ///
      segment_manager_type* segment_manager = m_mmap_manager->get_segment_manager();

      /// --- allocate a graphstore --- ///
      std::cout << "new graphstore_type" << std::endl;
      m_graphstore = new graphstore_type(segment_manager);

    } else {
      std::cout << "!!CAUTION!! not initializing allocators" << std::endl;
      m_graphstore = new graphstore_type(nullptr);
    }

  };

  ~DynamicGraphStoreNative()
  {
    delete m_mmap_manager;
    delete m_graphstore;
  };

  int insert(vertex_id_type src, vertex_id_type dst)
  {
    edge_property_type dummy;
    m_graphstore->insert_edge_dup(src, dst, dummy);
    return true;
  };

  int insert(vertex_id_type src, vertex_id_type dst, edge_property_type wg)
  {
    m_graphstore->insert_edge_dup(src, dst, wg);
    return true;
  };

  bool insert_uniquely(vertex_id_type src, vertex_id_type dst)
  {
    edge_property_type dummy;
    bool is_inserted = m_graphstore->insert_edge(src, dst, dummy);
    return is_inserted;
  };

  bool insert_uniquely(vertex_id_type src, vertex_id_type dst, edge_property_type wg)
  {
    bool is_inserted = m_graphstore->insert_edge(src, dst, wg);
    // std::cout << "insert " << src << " " << dst << " " << wg << " " << is_inserted << std::endl;
    return is_inserted;
  };

  size_t degree(vertex_id_type src)
  {
    return m_graphstore->degree(src);
  }

  void vertices()
  {
    auto tmp(m_graphstore->vertices_begin());
    std::memcpy(&m_vertex_iterator, &tmp, sizeof(typename graphstore_type::vertex_iterator));
  }

  vertex_id_type get_next_vertex()
  {
    vertex_id_type vrt;
    if (m_vertex_iterator != graphstore_type::vertices_end()) {
      vrt = m_vertex_iterator.source_vertex();
      ++m_vertex_iterator;
    }
    return vrt;
  }

  void edges(vertex_id_type src)
  {
    auto tmp(m_graphstore->adjacent_edge_begin(src));
    std::memcpy(&m_adjacent_edge_iterator, &tmp, sizeof(typename graphstore_type::adjacent_edge_iterator));
  }

  vertex_id_type get_next_edge()
  {
    vertex_id_type trg_vrt;
    if (m_adjacent_edge_iterator != graphstore_type::adjacent_edge_end(vertex_id_type())) {
      trg_vrt = m_adjacent_edge_iterator.target_vertex();
      // std::cout << "target edge is " << trg_vrt << std::endl;
      ++m_adjacent_edge_iterator;
    } else {
      std::cout << "!!!!! edge has reached END !!!!!" << std::endl;
    }
    return trg_vrt;
  }

  vertex_property_type get_vertex_property()
  {
    return m_vertex_iterator.property_data();
  }

  void set_vertex_property(vertex_property_type prop)
  {
    m_vertex_iterator.property_data() = prop;
  }

  void set_edge_property(edge_property_type prop)
  {
    /// m_adjacent_edge_iterator.property_data() = prop;
  }

  edge_property_type get_edge_property()
  {
    return m_adjacent_edge_iterator.property_data();
  }

  size_t num_vertices()
  {
    std::cout << "!!!!! this is a temporal implementation !!!!!!" << std::endl;
    return 1024;
  }

  size_t num_edges()
  {
    return m_graphstore->num_edges();
  }


 private:
  graphstore::utility::interprocess_mmap_manager* m_mmap_manager;
  graphstore_type* m_graphstore;

  typename graphstore_type::vertex_iterator m_vertex_iterator;
  typename graphstore_type::adjacent_edge_iterator m_adjacent_edge_iterator;
};

#endif

