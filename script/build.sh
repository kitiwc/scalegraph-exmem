#!/bin/sh

SCALEGRAPH_ROOT=$HOME/ScaleGraph
HAVOQGTGRAPHSTORE_ROOT=$HOME/havoqgt
BOOST_ROOT=$HOME/apps/boost_1_59_0

if [ ! -d "../build" ]; then
  mkdir ../build
fi

echo "Deleting all files under ../build"
rm -rf ../build/*

# Replace x10's original files
cp -r ../src/org/graphstore $SCALEGRAPH_ROOT/src/
cp ../src/org/WorkerPlaceGraph.x10 $SCALEGRAPH_ROOT/src/org/scalegraph/xpregel/WorkerPlaceGraph.x10
cp ../src/org/EdgeProvider.x10 $SCALEGRAPH_ROOT/src/org/scalegraph/xpregel/EdgeProvider.x10
cp ../src/org/XPregelGraph.x10 $SCALEGRAPH_ROOT/src/org/scalegraph/xpregel/XPregelGraph.x10
cp ../src/org/Vertex.x10 $SCALEGRAPH_ROOT/src/org/scalegraph/xpregel/Vertex.x10
cp ../src/org/MemoryChunkData.h $SCALEGRAPH_ROOT/src/org/scalegraph/util/MemoryChunkData.h

echo ""
CMT="x10c++ -VERBOSE_CHECKS -x10rt mpi -sourcepath $SCALEGRAPH_ROOT/src -d ../build -cxx-prearg -I$SCALEGRAPH_ROOT/include -cxx-prearg -I$HAVOQGTGRAPHSTORE_ROOT/include -cxx-prearg -I$BOOST_ROOT -cxx-prearg -fPIC -cxx-prearg -lrt -cxx-prearg -std=c++11 -g -O0 -o ../build/PageRankSimple -make -make-arg -j -v ../src/example/PageRankSimple.x10"
echo $CMT
$CMT
