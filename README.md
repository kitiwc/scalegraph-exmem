# X10のビルド #
X10のビルド。ScaleGraphではX10を少し改変した独自バージョン(SX10)を使っています。
gitで落としてください

```
#!bash

git clone https://github.com/scalegraph/sx10.git SX10
cd SX10

```

SX10はnewdevelopブランチを使ってください

```
#!bash

git checkout -b newdevelop origin/newdevelop

```

ビルドは x10.distディレクトリで行います

```
#!bash

cd x10.dist

```

antを使ってビルドします (要JAVA_HOMEの設定、GCはオフにします)

```
#!bash

ant -DDISABLE_GC=true -DX10RT_MPI=true dist
ant dist -DDISABLE_GC=true -DGCC_SYMBOLS=true -DNO_CHECKS=true -DX10RT_MPI=true -Doptimize=true -Davailable.procs=4
ant squeakyclean
```

SX10のビルド方法はX10本家と同じです。上記手順にてエラーが発生したなど、追加の情報は以下のページを参照してください。
http://x10-lang.org/x10-development/building-x10-from-source.html

パスを通します /path/to/SX10 はSX10ディレクトリへのパスです

```
#!bash

SX10_HOME=/path/to/SX10
export PATH=$SX10_HOME/x10.dist/bin:$PATH
export LD_LIBRARY_PATH=$SX10_HOME/x10.dist/lib:$SX10_HOME/x10.dist/stdlib/lib:$LD_LIBRARY_PATH
export C_INCLUDE_PATH=$SX10_HOME/x10.dist/include:$C_INCLUDE_PATH

```


-----------------------------------
# ScaleGraphのビルドとHavoqGTグラフストアの適用 #

## ScaleGraphのビルド ##
ScaleGraphをダウンロードします。ブランチはnewdevelopを使用します。
ビルドの必要はありません。

```
#!bash

git clone https://github.com/scalegraph/scalegraph.git ScaleGraph
cd ScaleGraph
git checkout -b newdevelop origin/newdevelop

```

## HavoqGTグラフストアをダウンロード ##
ブランチはdevelop_keitaです。

```
#!bash

git clone git@bitbucket.org:PerMA/havoqgt.git havoqgt
cd havoqgt
git checkout -b develop_keita origin/develop_keita

```

## Boost ##

Boostをhttp://www.boost.org/ からダウンロードし解凍します。
ヘッダファイルのみを使用しますのでビルドの必要はありません。

## HavoqGTグラフストアをScaleGraphに適用##

```
#!bash

git clone git@bitbucket.org:kitiwc/scalegraph-exmem.git scalegraph-exmem
cd scalegraph-exmem
```
ここで、scricpt/build.shのSCALEGRAPH_ROOTにScaleGraphのディレクトリを、
HAVOQGTGRAPHSTORE_ROOTにHavoqGTグラフストアのディレクトリを、
BOOST_ROOTにBoostのディレクトリを指定します。
それぞれのルートディレクトリへのパスを指定してください。
あとは、build.shを実行するのみです。

```
#!bash

cd ./script
sh build.sh

```
実行ファイルは../build/に生成されます。

-----------------------------------
# サンプルのPageRankプログラムを実行 #

```
#!bash

cd ../build
export X10_NTHREADS=1
mpirun -npN ./PageRankSimple /path/to/ScaleGraph/graphs/Weighted_RMAT_scale_10.csv

```
Nはプロセス数、プロセス辺りのスレッド数(X10_NTHREADS)は**必ず1**としてください。

-----------------------------------
# 実行確認済み環境 #
* GCC 4.6以降 (要C++11準拠)
* Oracle Java java version "1.7.X" もしくは IBM J9 VM java version "1.6.0"
* Boost 1_59_0以降
* MPI MVAPICH2

-----------------------------------
# その他のドキュメント #
# ビルド
https://docs.google.com/document/d/1MRtzeBN33yAAj4u0_jirYJxDc2j-Y2J9DvmEjp627I8/edit#
# サンプルプログラムとその引数
https://docs.google.com/spreadsheets/d/13wt8D3PDLuqTy4TTINWy_5hufsomG2w-5mqgj50SaEM/edit#gid=0


```
#ANT
export PATH=/path/to/ant/bin:$PATH

# JAVA
export JAVA_HOME=/path/to/java
export PATH=${JAVA_HOME}/bin:$PATH

#gcc
GCC=/path/to/gcc
export PATH="${GCC}/bin:${PATH}"
export LIBRARY_PATH="${GCC}/lib:${LIBRARY_PATH}"
export LD_LIBRARY_PATH="${GCC}/lib:${LD_LIBRARY_PATH}"
export LD_LIBRARY_PATH="${GCC}/lib64:${LD_LIBRARY_PATH}"
export MANPATH="${GCC}/share/man:${MANPATH}"
export C_INCLUDE_PATH="${GCC}/include/:${C_INCLUDE_PATH}"
export CPLUS_INCLUDE_PATH="${GCC}/include/:${C_INCLUDE_PATH}"

# mvapich2
MPI=/path/to/mvapich2
export PATH="${MPI}/bin:${PATH}"
export LIBRARY_PATH="${MPI}/lib:${LIBRARY_PATH}"
export LD_LIBRARY_PATH="${MPI}/lib:${LD_LIBRARY_PATH}"
export LD_LIBRARY_PATH="${MPI}/lib64:${LD_LIBRARY_PATH}"
export MANPATH="${MPI}/share/man:${MANPATH}"
export C_INCLUDE_PATH="${MPI}/include/:${C_INCLUDE_PATH}"
export CPLUS_INCLUDE_PATH="${MPI}/include/:${C_INCLUDE_PATH}"

# mvapich2 option
export GC_NPROCS=1
export X10RT_MPI_THREAD_MULTIPLE=true
export MV2_ENABLE_AFFINITY=0
export MV2_CPU_BINDING_LEVEL=socket
export MV2_CPU_BINDING_POLICY=scatter
export MV2_NUM_HCAS=2

#SX10
SX10_HOME=/path/to/SX10
export PATH=$SX10_HOME/x10.dist/bin:$PATH
export LD_LIBRARY_PATH=$SX10_HOME/x10.dist/lib:$SX10_HOME/x10.dist/stdlib/lib:$LD_LIBRARY_PATH
export C_INCLUDE_PATH=$SX10_HOME/x10.dist/include:$C_INCLUDE_PATH
```